<?php

namespace Tixcoredev\LiteralNumbers;


interface LiteralNumbersInterface
{
    public function toLiteral($int);
    public function setCurrency($currency);
    public function currency($bool);
}